import os.path
import shutil
from io import open
import glob
from tempfile import mkstemp
# try:
#     import windows_start
# except ModuleNotFoundError:
#     pass
from windowscomponents import path_locator


class SpecUpdater(object):
    ''' Modifies buildozer.spec and buildozer_extra.spec using bld_lines_add.spec'''

    def __init__(self, run=True):
        self.set_file_params()
        if run:
            self.run()

    def run(self):
        self.check_file_existence()
        self.split_files()
        self.create_objs()
        self.changed = False
        self.congruent = True
        for obj in self.obj_list:
            obj.update_spec()
            if obj.changed:
                self.changed = True
            if not obj.congruent:
                self.congruent = False
        self.reconsolidate_files()
        os.remove(self.bld_lines_add + self.ext)

    def set_file_params(self):
        self.ext = '.spec'
        self.split_line = '** ** ** extra spec ** ** **'
        self.bld_lines = 'bld_lines'
        self.bld_lines_add = 'bld_lines_add'
        self.old_prefix = 'bld_lines_old_'
        self.old_unmatched = 'bld_lines_old_unmatched'
        self.bs = 'buildozer'
        self.bse = 'buildozer_extra'

    def check_file_existence(self):
        self.file_exists = {}
        fl = ['bld_lines', 'bld_lines_add', 'bs', 'bse']
        for file in fl:
            self.file_exists[getattr(self, file)] = os.path.exists(file + self.ext)

    def split_files(self):
        files_to_split = [self.bld_lines, self.bld_lines_add]
        self.split_file_list = []
        for file in files_to_split:
            full_file = file + self.ext
            if not self.file_exists[file]:  # then create a file
                if file is self.bld_lines:
                    param_list = get_non_default_params(self.bs + self.ext)
                    param_list.append(self.split_line)
                    param_list += get_non_default_params(self.bse + self.ext)
                else:
                    param_list = []
                write_lines_file(param_list, full_file)
            f0 = file + '0' + self.ext  # base
            f1 = file + '1' + self.ext  # extra
            create_child_file(full_file, f0, stop=self.split_line)
            create_child_file(full_file, f1, start=self.split_line)
            self.split_file_list.append([f0, f1])

    def create_objs(self):
        base_files = [self.bs, self.bse]
        self.obj_list = []
        for i, b_file in enumerate(base_files):
            lines = self.split_file_list[0][i]
            lines_add = self.split_file_list[1][i]
            self.obj_list.append(SpecHandle(b_file + self.ext, lines_add, lines))

    def reconsolidate_files(self):
        if self.changed:
            if self.congruent:
                file_number = find_highest_old_file(self.old_prefix)
                shutil.move(self.bld_lines + self.ext, self.bld_lines + str(file_number) +
                            self.ext)
            else:
                shutil.move(self.bld_lines + self.ext, self.old_unmatched + self.ext)
            new_bld_file = self.bld_lines + self.ext
            write_lines_file(self.obj_list[0].param_list, new_bld_file)
            append_lines_file([self.split_line], new_bld_file)
            append_lines_file(self.obj_list[1].param_list, new_bld_file)
        for file_group in self.split_file_list:
            for file in file_group:
                os.remove(file)


class SpecHandle(object):
    ''' used to modify _add files to current full .spec files and current condensed files '''

    def __init__(self, main_file, lines_add_file, lines_file):
        self.main_spec_file = main_file
        self.changed = False
        self.congruent = True
        self.lines_add_file = lines_add_file
        self.lines_file = lines_file

    def update_spec(self):
        cur_params = BuildParams(self.main_spec_file)
        if not cur_params.exists:
            print('No file to use as ' + self.main_spec_file)
            return
        old_params = BuildParams(self.lines_file, False)
        new_params = BuildParams(self.lines_add_file, False)
        self.param_list = get_non_default_params(self.main_spec_file)
        if new_params.exists:
            if not dict1_is_subset_dict2(new_params.param_dict, cur_params.param_dict):
                if old_params.exists:
                    cur_to_old = SpecChanges(old_params.param_dict, cur_params.param_dict)
                    if cur_to_old.has_changed:  # non_std_dozer_lines.spec was not correct
                        print('previous ' + self.lines_file + ' is not congruent with ' +
                              self.main_file)
                        self.congruent = False
                new_to_cur = SpecChanges(new_params.param_dict, cur_params.param_dict)
                update_params_in_buildozer_spec(self.main_spec_file, new_to_cur,
                                                new_params.line_dict)
                self.changed = True
                print('\n' + self.main_spec_file + ' has been updated')
                #  use paramater list to write condensed file
                
                # write_lines_file(self.param_list, self.lines_file)
                print('Changes to ' + self.main_spec_file + ' are:')
                new_to_cur.print_changes()


class BuildParams(object):

    def __init__(self, spec, ignore_comments=True):
        self.spec = spec
        if not os.path.isfile(spec):
            print(spec + ' does not exist')
            self.exists = False
            return
        self.exists = True
        if ignore_comments:
            self.line_list = get_non_default_params(spec)
        else:
            self.line_list = get_non_default_params(spec, reject_leaders=[])
        self.param_dict, self.line_dict = parse_list(self.line_list)
        # self.print_param_dict()

    def print_param_dict(self):
        print('BOF', self.spec)
        for key, value in self.param_dict.items():
            print(key, value)
        print('EOF')


def get_non_default_params(spec, reject_leaders=['#']):
    """ Writes file 'ns' from non-standard 'bs' (buildozer.spec) options
        Does not write if write is False"""
    db_root = path_locator.get_dropbox_root()
    db_key = path_locator.DROPBOX_KEY
    result_list = get_multi_line_from_string(spec, '=', reject_leaders)
    for i, result in enumerate(result_list):
        result = clean_line_result(result, trailer_list=['\\n'])
        if db_key in result:
            result = result.replace(db_key, db_root)
        result_list[i] = result
    return result_list


def get_multi_line_from_string(file, find_string_list, reject_leader_list=[],
                               reject_string_list=[], start=None, stop=None):
    ''' Reads file and returns list of lines that are not excluded by reject lists '''
    return_list = []
    if start:
        reached_start = False
    else:
        reached_start = True
    with open(file, 'r', encoding='utf-8') as infile:
        # with open(file, 'rb') as infile:
        match_line = False
        for line in infile:
            if not reached_start:
                if start in line:
                    reached_start = True
                else:
                    continue
            if stop:
                if stop in line:
                    break
            for find_string in find_string_list:
                if find_string + " " in line:
                    match_line = True
                    for reject in reject_leader_list:
                        if reject == line.lstrip()[:len(reject)]:
                            match_line = False
                            break
                    if match_line:
                        for reject in reject_string_list:
                            if reject in line:
                                match_line = False
                                break
                        if match_line:
                            return_list.append(line)
    return return_list


def clean_line_result(result_string, new_start=None, leader_list=[], trailer_list=[]):
    ''' Cleans lines removing whitespace and other designated characters

    Starts trim at new start, then removes whitespaces and items from leader and trailer list'''
    if new_start:
        result_string = result_string[result_string.find(new_start):]
    result_string = result_string.strip()
    for i, _u in enumerate(leader_list):  # multiple runs are in case order is out
        for reject in leader_list:
            if reject == result_string[:len(reject)]:
                result_string = result_string[len(reject):].strip()
    for i, _u in enumerate(trailer_list):  # multiple runs are in case order is out
        for reject in trailer_list:
            if reject == result_string[:-len(reject)]:
                result_string = result_string[:-len(reject)].strip()
    return result_string


def create_child_file(parent_file_name, child_file_name, start=None, stop=None):
    ''' creates a child file which is a subset of a partent file '''
    if start:
        reached_start = False
    else:
        reached_start = True
    with open(child_file_name, 'w') as new_file:
        with open(parent_file_name, 'r') as old_file:
            for line in old_file:
                if not reached_start:
                    if start in line:
                        reached_start = True
                    else:
                        continue
                if stop:
                    if stop in line:
                        break
                new_file.write(line)


def update_params_in_buildozer_spec(file_path, spec_obj, line_dict):
    ''' Modifies buildozer.spec adding, removing, and modifying lines as spec_obj.action

    Opens buildozer.spec and creates temp file, reads old file in lines and writes from
    line_dict'''
    temp_file_obj, file_path_new = mkstemp()
    active_lines = []
    with open(file_path_new, 'w', encoding='utf-8') as new_file:
    # with open(file_path_new, 'wb') as new_file:
        with open(file_path, 'r', encoding='utf-8') as old_file:
        # with open(file_path, 'rb') as old_file:
            for line in old_file:
                changed = False
                if len(line.strip()) == 0:
                    pass
                elif line.strip()[0] == '#' and ('=' in line):
                    relevant_param = line[1:line.find('=')].strip()
                    for key in spec_obj.new:
                        # if key in relevant_param:  **1
                        if key == relevant_param:  # set to == to see to reduce duplications **1
                            new_file.write(line + line_dict[key] + '\n')
                            changed = True
                            break
                elif '=' in line:
                    relevant_param = line[:line.find('=')].strip()
                    if relevant_param in active_lines:  # line is duplicated
                        print(relevant_param, ' is a duplicate')
                        new_file.write((add_s_hash(line)))
                        changed = True
                    else:
                        active_lines.append(relevant_param)
                        for key in spec_obj.action:
                            # if key in line:  **1
                            if key == relevant_param:  # **1
                                if spec_obj.action[key] == 'remove':
                                    # new_file.write(unicode(add_s_hash(line)))
                                    new_file.write((add_s_hash(line)))
                                    changed = True
                                    break
                                elif spec_obj.action[key] == 'change':
                                    new_file.write(line_dict[key] + '\n')
                                    changed = True
                                    break
                                elif spec_obj.action[key] == 'new':
                                    print("\n\nError: cannot insert new line --> " +
                                          str(line_dict[key]) + "\nOld line -->" + str(line))
                if not changed:
                    new_file.write(line)
    os.close(temp_file_obj)
    os.remove(file_path)  # Remove original file
    shutil.move(file_path_new, file_path)  # Move new file


def write_lines_file(result_list, ns):
    with open(ns, 'wb') as newfile:
        for result in result_list:
            newfile.write((str(result) + '\n').encode('utf-8'))


def append_lines_file(result_list, ns):
    with open(ns, 'ab') as appendfile:
        for result in result_list:
            appendfile.write((str(result) + '\n').encode('utf-8'))


def parse_list(unparsed_list):
    ''' Parses uncommented lines into line_dict and parsed_dict

    Input is unparsed lines from file, returns two dicts,
    Line dict uses left side of '=' as key and line as value,
    Parsed_dict uses same key, byt rhs of '=' as value '''
    parsed_dict = {}
    line_dict = {}
    for line in unparsed_list:
        equal_place = line.find('=')
        if equal_place == -1:
            line += '= None'
        key = line[:equal_place].strip()
        value = line[equal_place + 1:].strip()
        if key in parsed_dict:
            key = '#! Duplicate: ' + str(key)
        parsed_dict[key] = duck_type_list(value)
        line_dict[key] = line
    return parsed_dict, line_dict


def duck_type_list(result_string):
    splitter = ", "
    if splitter in result_string:
        return result_string.split(splitter)
    else:
        return result_string


def dict1_is_subset_dict2(dict_1, dict_2):
    ''' Returns True if dict_1 is a subset of dict_2 '''
    for key, value in dict_1.items():
        if key not in dict_2.keys():
            return False
        if value != dict_2[key]:
            return False
    return True


def remove_s_hash(line):
    place = line.find('#')
    return line[place + 1:].strip()


def add_s_hash(line):
    return '#  ' + str(line)


def find_highest_old_file(template):
    trailer = '.spec'
    file_list = glob.glob(template + '*' + trailer)
    max_number = 1
    for file in file_list:
        number_str = file[len(template):-len(trailer)]
        try:
            number = int(number_str)
        except ValueError:
            continue
        if number > max_number:
            max_number = number
    return max_number


class SpecChanges(object):

    def __init__(self, new_dict, old_dict):
        self.new = {}
        self.change = {}
        self.remove = {}
        self.action = {}
        self.has_changed = False
        for i, (key, value) in enumerate(new_dict.items()):
            if '#' in key:
                key = remove_s_hash(key)
                self.remove[key] = value
                self.action[key] = 'remove'
            elif key not in old_dict.keys():
                self.new[key] = value
                self.action[key] = 'new'
            elif new_dict[key] != old_dict[key]:  # if value is changed
                self.change[key] = value
                self.action[key] = 'change'
        # self.print_changes()

    def print_changes(self):
        all_changes = self.new.copy()
        all_changes.update(self.change)
        all_changes.update(self.remove)
        for key, value in all_changes.items():
            print(key + ' ' + self.action[key] + ' ' + value)


if __name__ == "__main__":
    SpecUpdater()

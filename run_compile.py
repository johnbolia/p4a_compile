from __future__ import division
import os.path
import time
import subprocess
import glob
import sys
import shutil
from multiprocessing import Process, Queue
import json
DBC_PRINT = False

###################################################################################################
# This is a script for compiling a kivy project on windows in conjunction with virtualbox.
# It copies this file to an existing virtualbox guest session using pyvbox and the file
# copy_compile.py which is installed in the guest home dir.  It then copies the relevant directory
# code directory for the project to the guest machine and runs buildozer.  Upon complection, the
# .apk file is copied back to the windows machine, where it is then installed on the android device
# using ADB tools.
#
#         The requirements are:
#
#           pyvbox - https://pypi.python.org/pypi/pyvbox
#
#           Virtualbox with linux guest.  Buildozer must be installed on the guest.
#
#           Android Studio or some other version of ADB tools
#
#         The components of this script are:
#
#           buildozer_extra.spec sets the parameters for the script
#           See buildozer_tools.py and buildozer_extra.spec for further information.
#
#           make_jar() A script which fetches and compiles any jar files from a specified
#           directory.
#
#           copy_compile.py must be copied to the home directory of the virtualbox installation.
#           This script is run from pyvbox, currently the Virtualbox environment must be switched
#           on (but can be minimised) with a terminal window open.
#
#          To run the script:
#
#           python run_compile.py [options]
#
#               options:
#
#                   if omitted, runs debug script
#
#                   debug (default)
#                       increases version and asks for notes
#                       copies files from current windows directory
#                       runs debug build
#                       copies back to winddows directory
#                       installs onto android and runs
#
#                   no_inc
#                       Does not increase version number
#
#                   release
#                       As debug, but uses 'release' command
#                       
#
#                   make_jar
#                       As debug + Builds jar files from dirs specified in buildozer_extra.spec
#
#                   make_32bit
#                       makes a 32bit apk, default is 64bit
#
#                   new_project
#                       As debug + Creates new directory on VM
#
#                   build
#                       as debug, without installation onto android
#
#                   copy_to_vm
#                       copies files to vm, does not start build
#
#                   sign_only
#                       runs jarsigner, then return_copy_only
#
#                   return_copy_only
#                       Copies from VM to windows, installs, executes
#                       No build
#
#                   install
#                       Installs existing .apk to connected android device
#                       No build
#                       No install
#
#                   run_apk
#                       Only runs apk on connected android device
#                       No build
#                       No install
#
#                   no_purge
#                       previous code will not be deleted when copying new code to VM
#
#                   gc
#                       run on VboxManage guestcontrol less output, slightly quicker
# 
#                   help
#                       prints out this doc
################################################################################
exec_buildozer = ['buildozer', 'android']
EXEC_BUILDOZER_DEBUG = exec_buildozer + ['debug']
EXEC_BUILDOZER_RELEASE = exec_buildozer + ['release']

P4A_KEYS = {'P4A_RELEASE_KEYSTORE': '/home/anon/keystores/Anon.keystore',
            'P4A_RELEASE_KEYSTORE_PASSWD': 'anonanon',
            'P4A_RELEASE_KEYALIAS_PASSWD': 'anonanon',
            'P4A_RELEASE_KEYALIAS': 'anonAlias'}

DIR_KEYS = {'ANDROIDSDK': '/home/anon/Android/SDK',
            'ANDROIDNDK': '/home/anon/Android/android-ndk-r19c',
            'ANDROIDNDKVER': '17.2',
            'JAVA_HOME': '/usr/lib/jvm/java-1.8.0-openjdk-amd64'}

PATH_ADD = ['/home/anon/bin']

VER_FNAME = 'version/vsbike_version.csv'

# BUILD = {32: 'arch armeabi-v7a',
#          64: 'arch arm64-v8a'}


class VboxScript(object):
    """VboxScript exectutes the windows portion of the run_compile script.
    It parses and holds the parameters for buildozer_extra.spec.
    Also creates script_params.json which is copied to the VM
     """

    def __init__(self):
        """Parses buildozer_extra.spec and dispatches command based on arg parameters"""
        if 'help' in sys.argv:
            self.print_doc()
            return
        self.jars_exist = False
        self.timer = timer()
        print_help_hint()
        self.alt_vbox_dir = 'alt_dir' in sys.argv  # Allows alteranate windows dir
        self.make_32bit = 'make_32bit' in sys.argv
        self.new_project = 'new_project' in sys.argv
        self.return_copy_only = 'return_copy_only' in sys.argv
        self.copy_to_vm = 'copy_to_vm' in sys.argv
        self.sign_only = 'sign_only' in sys.argv
        self.release = 'release' in sys.argv
        self.no_purge = 'no_purge' in sys.argv
        self.inc_version = 'no_inc' not in sys.argv
        self.guestcontrol = 'gc' in sys.argv
        self.abbv_script = 'abbv_script' in sys.argv
        self.stop_queue = Queue()
        self.version_obj = self.handle_version(sys.argv)
        self.windows_params(sys.argv)
        if 'install' not in sys.argv and 'run_apk' not in sys.argv:
            self.copy_self()
        script_option_dict = {'build': 'run_vbox',
                              'install': 'send_to_android',
                              'run_apk': 'run_apk'
                              }
        full_script = True
        for key, value in script_option_dict.items():
            if key in sys.argv:
                full_script = False
                print('running:  ' + value)
                getattr(self, value)()
                self.timer.how_about_now()
        if full_script:
            print('running full script')
            self.run_full_script()
            if self.inc_version:
                self._append_notes()
        self.timer.how_about_now()

    def handle_version(self, args):
        for _arg in ['no_inc', 'install', 'copy_to_vm', 'return_copy_only', 'run_apk', 'sign_only']:
            if _arg in args:
                self.inc_version = False
        if self.abbv_script:
            return
        import version.version as version
        if self.inc_version:
            # if not version:
            #     print('This will fail as version has not been imported')
            # ask for input is False, since the version is created and then appended while compiling
            version.Version.add_version(self.release, ask_for_input=False, add_to_file=True, filename=VER_FNAME)
        return version.Version.get_latest_version_object(filename=VER_FNAME)

    def _append_notes(self):
        import version.version as version
        version.Version.overwrite_latest_notes(filename=VER_FNAME)
        self.copy_version_description()
        self.stop_queue.put(True)

    def windows_params(self, args):
        jdict = BuildParams(spec='run_compile.spec').param_dict
        # for j, k in jdict.items():
        #     print(f'{j}:{k}')
        jdict['windows_dir'] = dir_clean(os.getcwd())  # [os.getcwd().find('\\') + 1:]
        if 'vbox_dir' not in jdict or self.alt_vbox_dir is True:
            jdict['vbox_dir'] = dir_clean(os.getcwd()[os.getcwd().find('\\') + 1:])
        print('vbox_dir = ', jdict['vbox_dir'])
        # jdict['package_name'] = bs_dict['package.name']
        # jdict['title_unspaced'] = bs_dict['title'].replace(' ', '')
        jdict['title_unspaced'] = jdict['title'].replace(' ', '')
        # jdict['app_name'] = bs_dict['package.domain'] + '.' + bs_dict['package.name']
        jdict['app_name'] = jdict['package_domain'] + '.' + jdict['package_name']
        jdict['script_name'] = __file__
        if 'make_jar' in args:
            self.make_jar(jdict)
        jdict['jars_exist'] = self.jars_exist
        jdict['new_project'] = self.new_project
        jdict['return_copy_only'] = self.return_copy_only
        jdict['copy_to_vm'] = self.copy_to_vm
        jdict['release'] = self.release
        jdict['sign_only'] = self.sign_only
        jdict['make_32bit'] = self.make_32bit
        if self.make_32bit:
            jdict['exec_list'] = exec_list(self.version_obj, 32)
        else:
            jdict['exec_list'] = exec_list(self.version_obj, 64)
        jdict['no_purge'] = self.no_purge
        # for key, value in jdict.items():
        #     print(f'{key}: {value}')
        # 1/0
        with open('script_params.json', 'w') as fp:
            json.dump(jdict, fp)
        for key, value in jdict.items():
            setattr(self, key, value)

    def make_jar(self, jdict):
        """If 'make_jar' arg is used it will compile .jar files from parameters in buildozer.spec"""
        init_dir = os.getcwd()
        try:
            if 'jar_locations' in jdict:
                jar_locations = mdir_clean(jdict['jar_locations'])
                jar_dirs = mdir_clean(jdict['jar_dirs'])
                jar_names = mdir_clean(jdict['jar_names'])
            else:
                jar_locations = [dir_clean(jdict['jar_location'])]
                jar_dirs = [dir_clean(jdict['jar_dir'])]
                jar_names = [dir_clean(jdict['jar_name'])]
            for i, jar_location in enumerate(jar_locations):
                self.jars_exist = True
                jar_dir_root = dir_clean(jdict['jar_destination_windows'])
                jar_base_dir = jar_dirs[i]
                jar_name = jar_names[i]
                split_pt = jar_base_dir[:-1].find('/')
                jar_pre = jar_base_dir[:split_pt]
                new_dir = dir_clean(os.getcwd()) + jar_dir_root + jar_pre
                jar_dir = jar_dir_root + jar_base_dir
                new_dir_long = dir_clean(os.getcwd()) + jar_dir
                if os.path.exists(jar_dir_root + jar_pre):
                    shutil.rmtree(jar_dir_root + jar_pre)
                new_root = os.getcwd() + '/' + jar_dir_root[:-1]
                if not os.path.exists(new_root):
                            print('making ' + str(new_root))
                            os.makedirs(new_root)
                jar_tree = glob.glob(jar_location + jar_pre)[0]
                shutil.copytree(jar_tree, new_dir)
                dir_contents = glob.glob((new_dir_long + '/*'))
                discard_file_list = ['MainActivity', 'R.class', 'R$']
                discard_dir_list = ['/examples/']
                for file in dir_contents:
                    print(file)
                    keep = True
                    mfile = None
                    for name in discard_file_list:
                        bname = os.path.basename(file)
                        if bname[:len(name)] == name:
                            keep = False
                            break
                    if keep:
                        mfile = file.replace('\\', '/')  # depending how dir comes out
                        for dir_frag in discard_dir_list:
                            if dir_frag in mfile:
                                keep = False
                                break
                    if not keep:
                        print('removing ' + str(file))
                        os.remove(file)
                    else:
                        print('keeping:', bname, mfile, file)
                os.chdir(init_dir + '/' + jar_dir_root)
                cmd = 'jar cf ' + jar_name + ' ' + jar_base_dir
                s = subprocess.check_output(cmd.split())
                cmd = 'jar tf ' + jar_name
                s = subprocess.check_output(cmd.split())
                cmd = 'jar cf ' + jar_name + ' ' + jar_base_dir
                s = subprocess.check_output(cmd.split())
                cmd = 'jar tf ' + jar_name
                s = subprocess.check_output(cmd.split())
                os.chdir(init_dir)
        # except (AttributeError, IndexError, KeyError) as e:
        except ZeroDivisionError:
            print('No jars added')
            self.jars_exist = False
            os.chdir(init_dir)
            return

    def run_full_script(self):
        self.vbox_communicative = True
        Process(target=self.run_vbox).start()
        self.await_process = Process(target=self.awaiter, args=(self.stop_queue, self.timer.start))
        self.await_process.start()

    def run_vbox(self):
        """Runs vbox which enters keystroke onto the VM,
        only keystrokes are copy_compile_script.py"""
        try:
            import virtualbox
            vbox = virtualbox.VirtualBox()
        except ImportError:
            import vboxapi
            vbox = virtualbox.VirtualBox()
            # print('There is an error in the pyvbox installion')
            # raise
        vm = vbox.find_machine(self.VMname)
        session = vm.create_session()
        username, pwd = 'anon', 'anon'
        guest_session = self._new_copy(session, username, pwd)
        self._exec_script(session)
        guest_session.close()

    def _new_copy(self, session, username, pwd):
        import virtualbox
        guest_session = session.console.guest.create_session(username, pwd)
        p4_compile_dir = f'D:\\Dropbox\\Boxifier\\GitHub\\p4a_compile'
        f_run_compile = f'{p4_compile_dir}run_compile.py'
        script_params = f'{os.getcwd()}\\script_params.json'
        for dir_, file_ in [(p4_compile_dir, 'run_compile.py'), (os.getcwd(), 'script_params.json')]:
            src_file = f'{dir_}\\{file_}'
            dest_file = f'/home/anon/{file_}'
            guest_session.file_copy_to_guest(src_file, dest_file, [virtualbox.library.FileCopyFlag.none])
        return guest_session

    def _exec_script(self, session):
        session.console.keyboard.put_keys('\n \n \nsh a.sh\n')

    def awaiter(self, stop_queue, start_time):
        """Polls the current dir awaiting an .apk with a newer file modified time
        than when initialised"""
        if self.copy_to_vm:
            return  # then nothing to await
        f_time = self.get_mtime()
        while self.inc_version and not stop_queue.get():
            time.sleep(1)
        sys.stdout.write("waiting for .apk file")
        sys.stdout.flush()
        new_f_time = self.get_mtime()
        if self.new_project:
            wait_mins = 25
        else:
            wait_mins = 200
        incomplete = True
        count = int(time.time() - start_time)
        while incomplete and (time.time() < (start_time + wait_mins * 60)):
            count += 1
            time.sleep(1)
            new_f_time = self.get_mtime()
            if new_f_time > f_time:
                incomplete = False
            sys.stdout.write(".")
            if count / 10.0 == count // 10:
                sys.stdout.write(str(count) + 's')
            sys.stdout.flush()
            if not self.vbox_communicative:
                print('... will not arrive due to Vbox failure')
                return
        if incomplete:
            print("APK file has not arrived from virtualbox after %d seconds, quitting"
                  % (wait_mins * 60))
            return
        self.timer.how_about_now()
        self.send_to_android()

    def send_to_android(self, install=True, retry=True):
        """Installs and runs APK"""
        apk_path = self.apk_dir_32bit if self.make_32bit else self.apk_dir_64bit
        apk_name = max(glob.glob(apk_path + '\\*.apk'), key=os.path.getmtime)
        try:
            if install:
                print('\nSending to Android: ' + apk_name)
                cmd = 'adb install -r ' + apk_name
                s = subprocess.check_output(cmd.split())
                print(str(s).replace('\\r\\n', '\r\n'))
            print('\nRun program')
            cmd = 'adb shell am start -n ' + self.app_name.lower() + '/' + self.apk_activity
            s = subprocess.check_output(cmd.split())
            print(str(s).replace('\\r\\n', '\r\n'))
        except subprocess.CalledProcessError:
            if retry:
                self.send_to_android(install=True, retry=False)
            else:
                print("error: is the phone plugged in?")
        if retry:  # will only happen on outer loop, non-retry loop"""
            print_help_hint()
            self.timer.how_about_now()
            print(time.ctime())

    def run_apk(self):
        """Runs apk after install"""
        self.send_to_android(install=False)

    def get_mtime(self):
        """Get file modification time"""
        path = self.apk_dir_32bit if self.make_32bit else self.apk_dir_64bit
        file = get_latest_file(path + '\\*.apk', suppress_msg=True)
        if file is not None:
            return os.path.getmtime(file)
        return 0

    def copy_self(self):
        """Creates a copy of run_compile.py to copy onto the VM"""
        full_windows = self.windows_dir
        root_dir = full_windows[:full_windows[:-1].rfind('/') + 1]
        for f in [self.script_name, 'script_params.json']:
            src = full_windows + f
            dest = root_dir + f
            print(src, dest)
            shutil.copy2(src, dest)

    def copy_version_description(self):
        if hasattr(self, 'apk_dir_64bit'):
            root_apk_dir = self.apk_dir_64bit[:self.apk_dir_64bit.rfind('/')]
            ver_fname_without_path = os.path.basename(VER_FNAME)
            new_file = ver_fname_without_path.replace(".csv", "_autogen.csv")
            print(root_apk_dir, new_file)
            try:
                shutil.copy(f'{os.getcwd()}/{VER_FNAME}', f'{root_apk_dir}/{new_file}')
            except FileNotFoundError as e:
                print(f'Could not create {new_file}, {e}')

    def print_doc(self):
        with open('run_compile.py', 'r') as file:
            p_out = False
            line_list = []
            for line in file:
                if '#' * 20 in line:
                    p_out = not p_out
                    continue  # do not print this line
                if p_out:
                    line_list.append(line[1:])  # remove "#"
        print(''.join(line_list))


def print_help_hint():
    print('\n'*1 + '#'*50)
    print('\n Use extension: \n\n   help\n\n For a list of run_compile options\n')
    print('#'*50 + '\n'*1)


class VMbootstrap(object):
    """Bootstrap class of main script run on VM

    This class is copied into the VM from the vbox command 'python copy_compile_script.py'

    copy_compile_script.py is a short script which lives on the user/home of the VM
     - it copies:
                    run_compile.py
                    script_params.json

    All other tasks are then carried out by run_compile.py and build parameter information is
    carried in the file script_params.json
    """
    def __init__(self, windows_mount_drive=''):
        self.windows_mount_drive = windows_mount_drive  # '/media/sf_C_DRIVE/'

    def run(self):
        copy_to_subdir = True  # this is a switch, allows copying to subdirs on VBox
        copy_success = False
        print('script starting')
        # jdict contains all parameters and sets them as class attributes. Most of these are
        # contained in buildozer_extra.spec
        with open('script_params.json', 'r') as fp:
            jdict = json.load(fp)
        #  This sets the attrs self.vbox_dir, self.windows_dir, etc.
        for key, value in jdict.items():
            setattr(self, key, value)
        self.windows_mount_drive = f'/media/sf_{self.vbox_mount_drive}_DRIVE/'
        # Increase Gradle memory
        os.environ['GRADLE_OPTS'] = "-Xms1724m -Xmx5048m -Dorg.gradle.jvmargs='-Xms1724m -Xmx5048m'"
        # if self.release:
        #     # Update P4A_RELEASE keys
        for key, value in P4A_KEYS.items():
            os.environ[key] = value
        for key, value in DIR_KEYS.items():
            os.environ[key] = value
        # root directory of code on VM
        if self.make_32bit:
            vbox_dir = self.vbox_dir32
        else:
            vbox_dir = self.vbox_dir
        full_linux_dir = dir_clean(os.getcwd()) + dir_clean(vbox_dir)
        if not ch_or_mkdir(full_linux_dir, create_if_needed=self.new_project):
            print(str(full_linux_dir) + ' does not exist.\n')
            print("Use 'new_project' argument' at prompt to create new directories")
            return
        print(f'{"32 bit" if self.make_32bit else "64 bit"}', full_linux_dir)
        if hasattr(self, 'buildozer_ext_path'):
            apk_path = full_linux_dir + self.buildozer_ext_path + '/'
        else:
            apk_path = full_linux_dir
        # apk_path = full_linux_dir
        alt_apk_path = '/home/anon/python-for-android/'
        dbc_print('Apk path is: ' + str(apk_path))
        slash_place = self.windows_dir.find(':/') + 2
        windows_dir_share = self.windows_mount_drive + self.windows_dir[slash_place:]
        dbc_print('self.windows_mount_drive', self.windows_mount_drive)
        dbc_print('self.windows_dir[slash_place:]: ' + str(self.windows_dir[slash_place:]) +
                  '\nfull self.windows_dir: ' + str(self.windows_dir))
        # Set to True when 'install' argument is used, this will skip copying and build,
        # copying .apk from specified dir onto c:/dir
        if not self.return_copy_only and not self.sign_only:  # otherwise copy all code files over
            self.purge_old_files(full_linux_dir)
            copy_list = self.get_full_copy_dirs(windows_dir_share)
            # exclude files param is passed to glob_copy
            exclude_files = self.vbox_exclude_files.split(' ')
            for copy_option in copy_list:
                if '/' in copy_option and copy_to_subdir:
                    additional_path = str(copy_option[:copy_option.rfind('/')+1])
                    if not os.path.isdir(full_linux_dir + additional_path):
                        os.mkdir(full_linux_dir + additional_path)
                else:
                    additional_path = ''
                glob_copy((windows_dir_share + copy_option), full_linux_dir +
                          additional_path, exclude_files)
            copy_success = True
            # Execute buildozer string
            if self.copy_to_vm:  # your work here is done
                return
            # subprocess_with_display(EXEC_BUILDOZER_DEBUG)
            if not self.sign_only:
                print('about to run')
                exec_list_into_bash('p4a_script.sh', jdict['exec_list'])  # executes python for android
        if self.release or self.sign_only:
            release_script_into_bash(apk_path)
        filename = get_latest_file(apk_path + '*.apk')
        print('filename is: ' + str(filename))
        if filename:
            dest_dir = self.apk_dir_32bit if self.make_32bit else self.apk_dir_64bit
            share_dest_dir = self.windows_mount_drive + dest_dir[slash_place:]
            shutil.copy2(filename, share_dest_dir)
            return copy_success

    def purge_old_files(self, folder):
        if not self.no_purge:
            for the_file in os.listdir(folder):
                file_path = os.path.join(folder, the_file)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                    print(e)

    def get_full_copy_dirs(self, windows_dir_share):
        """Prepares a list of all directories except those specified by vbox_exclude_dir
        This prepares a list of files to copy base on wildcard + extension *.py for example,
        worth noting that exclude files is a different operation which is used to eliminate
        specific files"""
        # dbc_print('windows_dir_share', windows_dir_share)
        subdir_full_list = ([os.path.join(os.path.abspath(windows_dir_share), x[0])
                             for x in os.walk(os.path.abspath(windows_dir_share))])
        subdir_filtered_list = []
        for dir_ in subdir_full_list:
            inc = False
            for inc_dir in self.vbox_include_dirs:
                if inc_dir == dir_[len(windows_dir_share):]:
                    inc = True
                # if inc_dir in dir_.split('/'):
                #     inc = True
                #     # print('\n\nincluding ' + str(inc_dir))
            shortened_dir = dir_[len(windows_dir_share):] + '/'
            # if shortened_dir.count('/') > 1:
            #     inc = False  # we do not include subdirs
            if inc:
                # subdir_filtered_list.append(dir_[len(windows_dir_share):] + '/')
                subdir_filtered_list.append(shortened_dir)
                print('including ' + dir_ + ',   ' + shortened_dir)
        subdir_filtered_list.append('/')
        # dbc_print('subdir_filtered_list' + str(subdir_filtered_list))
        pre_copy_files = self.vbox_copy_files.split(' ')
        # dbc_print('pre_copy_files', pre_copy_files)
        copy_list = []
        for dir_ in subdir_filtered_list:
            for file in pre_copy_files:
                copy_list.append(dir_ + file)
        # dbc_print('copy_list', copy_list)
        for item in copy_list:
            print('f:  ', item)
        return copy_list

class BuildParams(object):

    def __init__(self, spec, ignore_comments=True):
        self.spec = spec
        if not os.path.isfile(spec):
            print(spec + ' does not exist')
            self.exists = False
            return
        self.exists = True
        if ignore_comments:
            self.line_list = get_non_default_params(spec)
        else:
            self.line_list = get_non_default_params(spec, reject_leaders=[])
        self.param_dict, self.line_dict = parse_list(self.line_list)
        # self.print_param_dict()

    def print_param_dict(self):
        print('BOF', self.spec)
        for key, value in self.param_dict.items():
            print(key, value)
        print('EOF')


class timer(object):
    """Used to measure operation times of compiling and copying"""

    def __init__(self):
        self.start = time.time()

    def how_about_now(self):
        time_str = "{0:.2f}".format(time.time() - self.start)
        print('Time elapsed: ' + time_str + 's')


def glob_copy(src, dest, exclude_files=None):
    """ copies files with glob * to dest
        dest should not contain filename"""
    dest_base_dir = os.path.dirname(dest)
    if not os.path.exists(dest_base_dir):
        print('making ' + str(dest_base_dir))
        os.makedirs(dest_base_dir)
    else:
        pass
        # dbc_print('already exists ' + str(dest_base_dir), dest)
    if dest[0] != '/':
        dest = '/' + dest
    for fname in glob.iglob(src):
        if os.path.basename(fname) in exclude_files:
            dbc_print('omitting :' + os.path.basename(fname))
            continue
        dbc_print('fname:', fname)
        final_source = os.path.join(dest, os.path.basename(fname))
        dbc_print('copying ' + fname + '\n to ' + final_source)
        shutil.copy2(fname, final_source)


def get_latest_file(search_path, suppress_msg=False):
    """Searches path and uses modified time to find the latest file, returns None if empty"""
    files = glob.glob(search_path)
    if not suppress_msg:
        print('search path: ' + str(search_path) + ' yields ' + str(files))
    # print(type(files))
    try:
        if len(files) > 0:
            return max(files, key=os.path.getmtime)
    except TypeError as e:
        if not suppress_msg:
            print(e, 'search path was ' + str(search_path))


def dir_clean(init_str, leader=False, trailer=True):
    """replaces \\ with /, adds a / leader if needed, adds a trailing / if asked for.
    Use mdir_clean for a list of strings"""
    if len(init_str) == 0:
        return init_str
    init_str = init_str.replace('\\', '/')
    if leader and (init_str[0] != "/"):
        init_str = "/" + init_str
    if trailer and (init_str[-1:] != "/"):
        init_str += "/"
    return init_str


def mdir_clean(init_str_list, leader=False, trailer=True):
    """applies dir_clean to a list"""
    clean_list = []
    for dir_str in init_str_list:
        clean_list.append(dir_clean(dir_str, leader, trailer))
    return clean_list


def ch_or_mkdir(dir_str, create_if_needed=True):
    """Changes working directory, creating the dir if needed"""
    dir_str = dir_clean(dir_str)
    cur_dir = dir_clean(os.getcwd())
    if cur_dir in dir_str:
        dir_extension = dir_str[len(cur_dir):]
    else:  # root not matching up, give it a single try
        return _ch_or_mkdir(dir_str, create_if_needed)
    for d in dir_extension.split('/'):
        new_dir_str = cur_dir + d
        successful = _ch_or_mkdir(new_dir_str, create_if_needed)
        if not successful:
            return False
        # print(cur_dir, type(str(d)), str(d))
        cur_dir = dir_clean(cur_dir + str(d))
    return True


def _ch_or_mkdir(dir_str, create_if_needed=True):
    """Sub command used by ch_or_mkdir, not to be exectuted by itself"""
    try:
        os.chdir(dir_str)
    except OSError:
        if create_if_needed:
            print(dir_str + " does not exist, attempting creation")
            try:
                os.mkdir(dir_str)
                os.chdir(dir_str)
            except OSError as e:
                print(e)
                print(dir_str + " does not exist, and could not be created")
                return False
        else:
            print(dir_str + " does not exist, use 'new_project' arg from run_compile.py" +
                            " to create this directory")
            return False
    return True


def subprocess_with_display(cmd_list, args=None):
    """Executes subprocess from cmd displaying output """
    print(cmd_list)
    p = subprocess.Popen(cmd_list, stdout=subprocess.PIPE)
    if args:
        p.communicate(input='y')
    for line in iter(p.stdout.readline, b''):
        print(('{}'.format(line.rstrip()))[2:])


def dbc_print(*args):
    """Used for printing information on file copies, set turned on to True"""
    turned_on = DBC_PRINT
    if turned_on:
        for arg in args:
            if not hasattr(arg, '__iter__') or isinstance(arg, str):
                print(arg)
            else:
                for a in arg:
                    print(a)


def exec_list(version_obj, bytes_type):
    output_list = []
    lead = ''
    nl = '\n'
    f_name = 'p4a_exec_32.spec' if bytes_type == 32 else 'p4a_exec.spec'
    with open(f_name, 'r') as f:
        for i, row in enumerate(f):
            if i == 2:
                lead = '--'
            # if row[:4] == 'arch':
            #     row = BUILD[bytes_type]
            if nl in row:
                row = row.replace(nl, '')
            # if row[:9] == 'dist_name':
            #     if bytes_type == 32:
            #         row = row.strip() + '32'
            if len(row) > 0:
                output_list.append(lead + row)
    # add release and version information
    if version_obj:
        output_list.insert(2, '--version=' + version_obj.get_version())
        output_list.insert(2, '--' + version_obj.get_release_str())
    return output_list


def exec_list_into_bash(f_name, _exec_list, release=False):
    long_str = ' '.join(_exec_list)
    print('p4a command:\n')
    print(long_str)
    sh_script = 'svar="' + long_str + '"\neval $svar'
    sh_script = 'PATH="/home/anon/bin/:$PATH"\n' + sh_script
    write_temp_bash(sh_script, f_name)


def release_script_into_bash(apk_path):
    filename = get_latest_file(apk_path + '*.apk')
    pwd = 'anon'
    sudo_str = 'echo ' + pwd + ' | sudo -S '
    jarsign_str = 'jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $P4A_RELEASE_KEYSTORE'
    storepass_str = ' -storepass $P4A_RELEASE_KEYSTORE_PASSWD '
    keyalias = ' $P4A_RELEASE_KEYALIAS'
    cmd_line = sudo_str + jarsign_str + storepass_str + filename + keyalias + ' \n'
    f_name = 'jarsigner_bash.sh'
    print('\n\n\n\n')
    print(cmd_line)
    write_temp_bash(cmd_line, f_name)
    new_filename = filename.replace('-unsigned', '')
    zip_align_str = DIR_KEYS['ANDROIDSDK'] + '/build-tools/28.0.3/zipalign -v -f 4 '
    full_cmd_list = [zip_align_str, filename, new_filename]
    print(' '.join(full_cmd_list))
    write_temp_bash(' '.join(full_cmd_list))
    # try:
    #     subprocess_with_display(full_cmd_list)
    # except OSError:
    #     time.sleep(1)
    #     subprocess_with_display(full_cmd_list)


def write_temp_bash(bash_str, f_name='temp_bash.sh'):
    full_str = '#!/bin/bash\n' + bash_str
    with open(f_name, 'w') as f:
        f.write(full_str)
    subprocess_with_display(['sh', f_name])
    # os.remove(f_name)


def parse_list(unparsed_list):
    ''' Parses uncommented lines into line_dict and parsed_dict

    Input is unparsed lines from file, returns two dicts,
    Line dict uses left side of '=' as key and line as value,
    Parsed_dict uses same key, byt rhs of '=' as value '''
    parsed_dict = {}
    line_dict = {}
    for line in unparsed_list:
        equal_place = line.find('=')
        if equal_place == -1:
            line += '= None'
        key = line[:equal_place].strip()
        value = line[equal_place + 1:].strip()
        if key in parsed_dict:
            key = '#! Duplicate: ' + str(key)
        parsed_dict[key] = duck_type_list(value)
        line_dict[key] = line
    return parsed_dict, line_dict


def duck_type_list(result_string):
    splitter = ", "
    if splitter in result_string:
        return result_string.split(splitter)
    else:
        return result_string


def get_non_default_params(spec, reject_leaders=['#']):
    """ Writes file 'ns' from non-standard 'bs' (buildozer.spec) options
        Does not write if write is False"""
    from windowscomponents import path_locator
    db_root = path_locator.get_dropbox_root()
    db_key = path_locator.DROPBOX_KEY
    result_list = get_multi_line_from_string(spec, '=', reject_leaders)
    for i, result in enumerate(result_list):
        result = clean_line_result(result, trailer_list=['\\n'])
        if db_key in result:
            result = result.replace(db_key, db_root)
        result_list[i] = result
    return result_list


def clean_line_result(result_string, new_start=None, leader_list=[], trailer_list=[]):
    ''' Cleans lines removing whitespace and other designated characters

    Starts trim at new start, then removes whitespaces and items from leader and trailer list'''
    if new_start:
        result_string = result_string[result_string.find(new_start):]
    result_string = result_string.strip()
    for i, _u in enumerate(leader_list):  # multiple runs are in case order is out
        for reject in leader_list:
            if reject == result_string[:len(reject)]:
                result_string = result_string[len(reject):].strip()
    for i, _u in enumerate(trailer_list):  # multiple runs are in case order is out
        for reject in trailer_list:
            if reject == result_string[:-len(reject)]:
                result_string = result_string[:-len(reject)].strip()
    return result_string


def get_multi_line_from_string(file, find_string_list, reject_leader_list=[],
                               reject_string_list=[], start=None, stop=None):
    ''' Reads file and returns list of lines that are not excluded by reject lists '''
    return_list = []
    if start:
        reached_start = False
    else:
        reached_start = True
    with open(file, 'r', encoding='utf-8') as infile:
        # with open(file, 'rb') as infile:
        match_line = False
        for line in infile:
            if not reached_start:
                if start in line:
                    reached_start = True
                else:
                    continue
            if stop:
                if stop in line:
                    break
            for find_string in find_string_list:
                if find_string + " " in line:
                    match_line = True
                    for reject in reject_leader_list:
                        if reject == line.lstrip()[:len(reject)]:
                            match_line = False
                            break
                    if match_line:
                        for reject in reject_string_list:
                            if reject in line:
                                match_line = False
                                break
                        if match_line:
                            return_list.append(line)
    return return_list


if __name__ == "__main__":
    if 'run' in sys.argv:
        s = VMbootstrap()
        s.run()
    else:
        s = VboxScript()
